#! /bin/bash
# Usage: pdfdex -[|h|csv|html|] <path to top of PDF hierarchy>
# Defaults to text report unless first argument is "csv" or "html"
# Requires pdfinfo from poppler-utils package 
# Begun 2/27/2017
# Last Update 2/27/2017
# silent700 at Google's Mail Service

tvar1=`cat /dev/urandom | tr -dc 'a-z' | fold -w 8 | head -n 1`
tvar2=`cat /dev/urandom | tr -dc 'a-z' | fold -w 8 | head -n 1`

case "$1" in 
-html) 
 find $2 | grep -i pdf > /tmp/$tvar1
 while read FIL
 do
  if [ "$FIL" != "" ]; then
   DUMP=`pdfinfo $FIL`
   X=`echo "$DUMP" | grep Author:`
   Y=`echo "$DUMP" | grep Title:`
   Z=`echo "$DUMP" | grep Pages:`
   echo ${X:16}~${Y:16}~${Z:16}~${FIL:20} >> /tmp/$tvar2
  fi
 done < /tmp/$tvar1
 rm /tmp/$tvar1
 sort -d /tmp/$tvar2 > /tmp/$tvar1
 rm /tmp/$tvar2
# read sorted CSV file into array to process each item
 OIFS=IFS
 IFS='~'
 echo "<table border=1>"
 echo "<tr><td>Author</td><td>Title</td><td>Pages</td><td>Path</td></tr>"
 while read -ra ITEM; do
  echo "<tr>"
  echo "<td>"${ITEM[0]}"</td>"
  echo "<td>"${ITEM[1]}"</td>"
  echo "<td>"${ITEM[2]}"</td>"
  echo "<td>"${ITEM[3]}"</td>"
  echo "</tr>"
 done < /tmp/$tvar1
 echo "</table>"
 rm /tmp/$tvar1 
;;

-csv)
 find $2 | grep -i pdf > /tmp/$tvar1
 echo "Author,Title,Pages,Path"
 while read FIL
 do
  if [ "$FIL" != "" ]; then
   DUMP=`pdfinfo $FIL`
   X=`echo "$DUMP" | grep Author:`
   Y=`echo "$DUMP" | grep Title:`
   Z=`echo "$DUMP" | grep Pages:`
   echo ${X:16}~${Y:16}~${Z:16}~$FIL
  fi
 done < /tmp/$tvar1
 rm /tmp/$tvar1
;;

 -*|"")
 echo "Usage  pdfdex -html <input path> HTML-formatted report"
 echo "       pdfdex -csv <input path> character-separated value report"
 echo "       pdfdex <input path> plain text report"
 echo "       pdfdex -h this help text"
;;

*)
 # plain text output 
 find $1 | grep -i pdf > /tmp/$tvar1
 while read FIL
 do
  echo $FIL
  DUMP=`pdfinfo $FIL`
  X=`echo "$DUMP" | grep Author:`
  Y=`echo "$DUMP" | grep Title:`
  Z=`echo "$DUMP" | grep Pages:`
  echo Author: ${X:16}
  echo Title: ${Y:16}
  echo ${Z:16} pages
  echo - - - - -
 done < /tmp/$tvar1
 rm /tmp/$tvar1
;;
esac
